#include <gtest/gtest.h>
#include "basic_types.hpp"

using namespace homccube;

TEST(Coord3, sizeof) {
  ASSERT_EQ(4, sizeof(Coord<3>));
}

TEST(Coord3, getset) {
  Coord<3> x;
  x[0] = 4;
  x[1] = -1;
  x[2] = 1022;
  ASSERT_EQ(4, x[0]);
  ASSERT_EQ(-1, x[1]);
  ASSERT_EQ(1022, x[2]);
}

TEST(BasicTypes3, valid_shape) {
  EXPECT_TRUE(BasicTypes<3>::valid_shape(std::vector<int>{1, 1, 1}));
  
  // Maximum pixels are 1**29 - 4
  EXPECT_TRUE(BasicTypes<3>::valid_shape(std::vector<int>{1023, 1024, 512}));
  EXPECT_FALSE(BasicTypes<3>::valid_shape(std::vector<int>{1024, 1024, 512}));
  EXPECT_FALSE(BasicTypes<3>::valid_shape(std::vector<int>{1025, 1024, 512}));

  // shape[0] < 2046, shape[1] < 2046, shape[2] < 1022
  EXPECT_TRUE(BasicTypes<3>::valid_shape(std::vector<int>{2045, 510, 510}));
  EXPECT_FALSE(BasicTypes<3>::valid_shape(std::vector<int>{2046, 510, 510}));
  EXPECT_TRUE(BasicTypes<3>::valid_shape(std::vector<int>{510, 2045, 510}));
  EXPECT_FALSE(BasicTypes<3>::valid_shape(std::vector<int>{510, 2046, 510}));
  EXPECT_TRUE(BasicTypes<3>::valid_shape(std::vector<int>{510, 510, 1021}));
  EXPECT_FALSE(BasicTypes<3>::valid_shape(std::vector<int>{510, 510, 1022}));
}

TEST(Coord3, initializer_list) {
  auto x = BasicTypes<3>::build_coord({4, -1, 1022});
  ASSERT_EQ(4, x[0]);
  ASSERT_EQ(-1, x[1]);
  ASSERT_EQ(1022, x[2]);
}

TEST(BasicTypes3, empty_coord) {
  auto e = BasicTypes<3>::empty_coord();
  ASSERT_FALSE(BasicTypes<3>::valid_shape(std::vector<int>{e[0], e[1], e[2]}))
      << coord2s<3>(e);
}

TEST(BasicTypes3, root) {
  auto r = BasicTypes<3>::root_coord();
  ASSERT_FALSE(BasicTypes<3>::valid_shape(std::vector<int>{r[0], r[1], r[2]}))
      << coord2s<3>(r);
}
