#include <gtest/gtest.h>
#include "cube_map.hpp"
#include "bitmap.hpp"

using namespace homccube;

using Axis = BasicTypes<3>::Axis;
using ND = BasicTypes<3>::ND;
template<int dim>
Cube<3, dim> cube3(Axis x, Axis y, Axis z, ND nd) {
  return Cube<3, dim>(BasicTypes<3>::build_coord({x, y, z}), nd, 0);
}

TEST(CubeMap, CubeMap) {
  CubeMap<3, 1, int> cube_map(Shape<3>{4, 5, 3});
}

TEST(CubeMap, num_vertices_) {
  CubeMap<3, 1, int> cube_map(Shape<3>{4, 5, 3});
  EXPECT_EQ(60, cube_map.num_vertices_);
}

TEST(CubeMap, ndsize) {
  EXPECT_EQ(1, static_cast<int>(CubeMap<3, 0, int>::ndsize));
  EXPECT_EQ(3, static_cast<int>(CubeMap<3, 1, int>::ndsize));
  EXPECT_EQ(3, static_cast<int>(CubeMap<3, 2, int>::ndsize));
  EXPECT_EQ(1, static_cast<int>(CubeMap<3, 3, int>::ndsize));
}

TEST(CubeMap, index) {
  Shape<3> shape{4, 5, 3};
  CubeMap<3, 1, int> cube_map(shape, 0);
  EXPECT_EQ(3 * 15 + 3 * 3, cube_map.index(cube3<1>(3, 3, 0, 0b001)));
}

TEST(CubeMap, getset) {
  CubeMap<3, 1, int> cube_map(Shape<3>{4, 5, 3}, -1);
  EXPECT_EQ(-1, cube_map[cube3<1>(3, 3, 0, 0b001)]);
  cube_map[cube3<1>(3, 3, 0, 0b001)] = 1;
  cube_map[cube3<1>(3, 3, 0, 0b010)] = 2;
  cube_map[cube3<1>(3, 3, 0, 0b100)] = 3;
  EXPECT_EQ(1, cube_map[cube3<1>(3, 3, 0, 0b001)]);
  EXPECT_EQ(2, cube_map[cube3<1>(3, 3, 0, 0b010)]);
  EXPECT_EQ(3, cube_map[cube3<1>(3, 3, 0, 0b100)]);
}
