#include <gtest/gtest.h>
#include "link_find.hpp"

using namespace homccube;

TEST(LinkFind, LinkFind) {
  Bitmap<3> bitmap(std::vector<int>{3, 5, 4});
  LinkFind<3> link_find(bitmap);
}

TEST(LinkFind, compute) {
  static const auto C = BasicTypes<3>::build_coord;
  static const auto INFINITE_LEVEL = BasicTypes<3>::INFINITE_LEVEL;
  
  Bitmap<3> bitmap(std::vector<int>{2, 3, 3});
  bitmap.load({
      0, 3, 8,
      4, 16, 5,
      13, 11, 6,

      1, 9, 7,
      15, 17, 10,
      2, 12, 14,
    });
  
  LinkFind<3> link_find(bitmap);
  link_find.compute();

  ASSERT_EQ(4, link_find.birth_levels_.size());
  ASSERT_EQ(4, link_find.death_levels_.size());
  EXPECT_EQ(0, link_find.birth_levels_[0]);
  EXPECT_EQ(INFINITE_LEVEL, link_find.death_levels_[0]);
  EXPECT_EQ(7, link_find.birth_levels_[1]);
  EXPECT_EQ(8, link_find.death_levels_[1]);
  EXPECT_EQ(5, link_find.birth_levels_[2]);
  EXPECT_EQ(8, link_find.death_levels_[2]);
  EXPECT_EQ(2, link_find.birth_levels_[3]);
  EXPECT_EQ(12, link_find.death_levels_[3]);

  ASSERT_EQ((9 + 12 * 2) - 17, link_find.survivors_->size());
  EXPECT_EQ(bitmap.cube<1>(C({1, 0, 0}), C({1, 0, 1})), (*link_find.survivors_)[0]);
  EXPECT_EQ(bitmap.cube<1>(C({1, 0, 1}), C({1, 0, 2})), (*link_find.survivors_)[1]);
  EXPECT_EQ(bitmap.cube<1>(C({1, 0, 2}), C({1, 1, 2})), (*link_find.survivors_)[2]);
  EXPECT_EQ(bitmap.cube<1>(C({0, 2, 0}), C({1, 2, 0})), (*link_find.survivors_)[3]);
  EXPECT_EQ(bitmap.cube<1>(C({0, 2, 0}), C({0, 2, 1})), (*link_find.survivors_)[4]);
  EXPECT_EQ(bitmap.cube<1>(C({1, 1, 2}), C({1, 2, 2})), (*link_find.survivors_)[5]);
  EXPECT_EQ(bitmap.cube<1>(C({1, 2, 1}), C({1, 2, 2})), (*link_find.survivors_)[6]);
  EXPECT_EQ(bitmap.cube<1>(C({1, 0, 0}), C({1, 1, 0})), (*link_find.survivors_)[7]);
  EXPECT_EQ(bitmap.cube<1>(C({1, 1, 0}), C({1, 2, 0})), (*link_find.survivors_)[8]);
  EXPECT_EQ(bitmap.cube<1>(C({0, 1, 1}), C({0, 2, 1})), (*link_find.survivors_)[9]);
  EXPECT_EQ(bitmap.cube<1>(C({0, 1, 0}), C({0, 1, 1})), (*link_find.survivors_)[10]);
  EXPECT_EQ(bitmap.cube<1>(C({0, 1, 1}), C({0, 1, 2})), (*link_find.survivors_)[11]);
  EXPECT_EQ(bitmap.cube<1>(C({1, 0, 1}), C({1, 1, 1})), (*link_find.survivors_)[12]);
  EXPECT_EQ(bitmap.cube<1>(C({1, 1, 1}), C({1, 2, 1})), (*link_find.survivors_)[13]);
  EXPECT_EQ(bitmap.cube<1>(C({1, 1, 0}), C({1, 1, 1})), (*link_find.survivors_)[14]);
  EXPECT_EQ(bitmap.cube<1>(C({1, 1, 1}), C({1, 1, 2})), (*link_find.survivors_)[15]);
  

  for (int i = 0; i < 14; ++i)
    EXPECT_TRUE((*link_find.survivors_)[i].level_ < (*link_find.survivors_)[i + 1].level_)
        << i << " " << (*link_find.survivors_)[i] << " " << (*link_find.survivors_)[i + 1];
}

TEST(LinkFind2, compute) {
  static const auto C = BasicTypes<2>::build_coord;
  static const auto INFINITE_LEVEL = BasicTypes<2>::INFINITE_LEVEL;

  Bitmap<2> bitmap(std::vector<int>{3, 3});
  bitmap.load({
      0, 1, 2,
      6, 8, 7,
      3, 4, 5
    });

  LinkFind<2> link_find(bitmap);
  link_find.compute();

  ASSERT_EQ(2, link_find.birth_levels_.size());
  EXPECT_EQ(0, link_find.birth_levels_[0]);
  EXPECT_EQ(INFINITE_LEVEL, link_find.death_levels_[0]);
  EXPECT_EQ(3, link_find.birth_levels_[1]);
  EXPECT_EQ(6, link_find.death_levels_[1]);
  
  auto survivors = link_find.survivors_;
  ASSERT_EQ(4, survivors->size());
  for (int i = 0; i < 3; ++i)
    EXPECT_TRUE(survivors->at(i) < survivors->at(i + 1));
  EXPECT_EQ(bitmap.cube<1>(C({1, 2}), C({2, 2})), survivors->at(0));
  EXPECT_EQ(bitmap.cube<1>(C({1, 0}), C({1, 1})), survivors->at(2));
  EXPECT_EQ(bitmap.cube<1>(C({1, 1}), C({2, 1})), survivors->at(1));
  EXPECT_EQ(bitmap.cube<1>(C({1, 1}), C({1, 2})), survivors->at(3));
}
