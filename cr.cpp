#include <fstream>
#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <vector>

#include "bitmap.hpp"
#include "link_find.hpp"
#include "reduce.hpp"
#include "dipha_format.hpp"
#include "dipha.hpp"

using namespace homccube;

void show_help() {
  std::cout << "Usage: cr CACHE_STRATEGY INPUT OUTPUT" << std::endl;
  std::exit(1);
}

template<int maxdim>
void check_shape(const std::vector<int>& shape) {
  if (!BasicTypes<maxdim>::valid_shape(shape)) {
    std::cerr << "The shape of input data is not valid" << std::endl;
    exit(2);
  }
}

template<int maxdim>
void run(const std::vector<int>& shape, const Config& config,
         std::istream* in, std::ostream* out) {
  using dipha::compute_0th_pd;
  using dipha::compute_kth_pd;

  Bitmap<maxdim> bitmap(shape);
  bitmap.load(in);

  dipha::skip_diagram_header(out);
  uint64_t num_pairs = 0;
  std::cout << "PD0" << std::endl;
  auto survivors0 = compute_0th_pd(bitmap, out, &num_pairs);
  std::cout << "PD1" << std::endl;
  auto survivors1 = compute_kth_pd(bitmap, survivors0, config, out, &num_pairs);
  std::cout << "PD2" << std::endl;
  auto survivors2 = compute_kth_pd(bitmap, survivors1, config, out, &num_pairs);
  std::cout << "PD3" << std::endl;
  auto survivors3 = compute_kth_pd(bitmap, survivors2, config, out, &num_pairs);
  std::cout << "PD4" << std::endl;
  auto survivors4 = compute_kth_pd(bitmap, survivors3, config, out, &num_pairs);
  
  out->seekp(0, std::ios_base::beg);
  dipha::write_diagram_header(num_pairs, out);
}

int main(int argc, char** argv) {
  if (argc != 4)
    show_help();

  Config config = {strategy_from_string(std::string(argv[1])), 0};
  if (config.cache_strategy_ == CacheStrategy::UNKNOWN) {
    std::cerr << "Unknown Cache Strategy: " << argv[1] << std::endl;
    exit(2);
  }

  std::ifstream fin(argv[2], std::ios::binary);
  auto shape = dipha::read_metadata(&fin);

  if (shape.size() == 2) check_shape<2>(shape);
  if (shape.size() == 3) check_shape<3>(shape);
  if (shape.size() == 4) check_shape<4>(shape);

  std::ofstream fout(argv[3], std::ios::binary);

  if (shape.size() == 2) run<2>(shape, config, &fin, &fout);
  if (shape.size() == 3) run<3>(shape, config, &fin, &fout);
  if (shape.size() == 4) run<4>(shape, config, &fin, &fout);
  
  return 0;
}
