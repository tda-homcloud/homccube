#include <gtest/gtest.h>
#include "reduce.hpp"
#include "link_find.hpp"

using namespace homccube;

class ReducerTest: public ::testing::TestWithParam<Config> {
};


TEST_P(ReducerTest, compute_1) {
  auto C = BasicTypes<3>::build_coord;
  Config config = GetParam();
  
  Bitmap<3> bitmap(std::vector<int>{1, 3, 3});
  bitmap.load({
      0, 1, 2,
      6, 8, 7,
      3, 4, 5
    });
  LinkFind<3> link_find(bitmap);
  link_find.compute();

  auto survivors = link_find.survivors_;
  ASSERT_EQ(4, survivors->size());
  for (int i = 0; i < 3; ++i)
    EXPECT_TRUE(survivors->at(i) < survivors->at(i + 1));
  EXPECT_EQ(bitmap.cube<1>(C({0, 1, 2}), C({0, 2, 2})), survivors->at(0));
  EXPECT_EQ(bitmap.cube<1>(C({0, 1, 0}), C({0, 1, 1})), survivors->at(2));
  EXPECT_EQ(bitmap.cube<1>(C({0, 1, 1}), C({0, 2, 1})), survivors->at(1));
  EXPECT_EQ(bitmap.cube<1>(C({0, 1, 1}), C({0, 1, 2})), survivors->at(3));
  
  Reducer<3, 1> reducer(bitmap, link_find.survivors_, config);
                        
  reducer.compute();

  ASSERT_EQ(1, reducer.birth_levels_.size());
  EXPECT_EQ(7, reducer.birth_levels_[0]);
  EXPECT_EQ(8, reducer.death_levels_[0]);
  ASSERT_EQ(0, reducer.upper_survivors()->size());
}

TEST_P(ReducerTest, compute_2) {
  Config config = GetParam();

  Bitmap<3> bitmap(std::vector<int>{2, 3, 3});
  bitmap.load({
      0, 3, 8,
      4, 16, 5,
      13, 11, 6,

      1, 9, 7,
      15, 17, 10,
      2, 12, 14,
    });
  LinkFind<3> link_find(bitmap);
  link_find.compute();

  Reducer<3, 1> reducer(bitmap, link_find.survivors_, config);
  reducer.compute();

  ASSERT_EQ(1, reducer.birth_levels_.size());
  EXPECT_EQ(13, reducer.birth_levels_[0]);
  EXPECT_EQ(16, reducer.death_levels_[0]);
  ASSERT_EQ(4, reducer.upper_survivors()->size());
}

TEST_P(ReducerTest, compute_3) {
  Config config = GetParam();

  Bitmap<3> bitmap(std::vector<int>{3, 3, 3});
  bitmap.load({
      0 , 12, 1 ,
      10, 25, 19,
      2 , 15, 3 ,

      8 , 18, 13,
      22, 26, 23,
      9 , 21, 17,
      
      4 , 14, 5 ,
      11, 24, 20,
      6 , 16, 7 ,
    });
  LinkFind<3> link_find(bitmap);
  link_find.compute();

  Reducer<3, 1> reducer1(bitmap, link_find.survivors_, config);
  reducer1.compute();

  ASSERT_EQ(5, reducer1.birth_levels_.size());
  EXPECT_EQ(20, reducer1.birth_levels_[0]);
  EXPECT_EQ(23, reducer1.death_levels_[0]);
  EXPECT_EQ(19, reducer1.birth_levels_[1]);
  EXPECT_EQ(24, reducer1.death_levels_[1]);
  EXPECT_EQ(17, reducer1.birth_levels_[2]);
  EXPECT_EQ(21, reducer1.death_levels_[2]);
  EXPECT_EQ(14, reducer1.birth_levels_[3]);
  EXPECT_EQ(18, reducer1.death_levels_[3]);
  EXPECT_EQ(11, reducer1.birth_levels_[4]);
  EXPECT_EQ(22, reducer1.death_levels_[4]);
  ASSERT_EQ(8, reducer1.upper_survivors()->size());

  Reducer<3, 2> reducer2(bitmap, reducer1.upper_survivors(), config);
                         
  reducer2.compute();
  ASSERT_EQ(1, reducer2.birth_levels_.size());
  EXPECT_EQ(25, reducer2.birth_levels_[0]);
  EXPECT_EQ(26, reducer2.death_levels_[0]);
  ASSERT_EQ(0, reducer2.upper_survivors()->size());
}

TEST_P(ReducerTest, compute_4) {
  Config config = GetParam();
  static const auto INFINITE_LEVEL = BasicTypes<3>::INFINITE_LEVEL;

  Bitmap<3> bitmap(std::vector<int>{2, 2, 2}, std::bitset<3>(0b111));
  bitmap.load({
      1, 2,
      3, 4,
      5, 6,
      7, 8,
    });
  LinkFind<3> link_find(bitmap);
  link_find.compute();

  Reducer<3, 1> reducer1(bitmap, link_find.survivors_, config);
                         
  reducer1.compute();

  ASSERT_EQ(3, reducer1.birth_levels_.size());
  EXPECT_EQ(5 - 1, reducer1.birth_levels_[0]);
  EXPECT_EQ(INFINITE_LEVEL, reducer1.death_levels_[0]);
  EXPECT_EQ(3 - 1, reducer1.birth_levels_[1]);
  EXPECT_EQ(INFINITE_LEVEL, reducer1.death_levels_[1]);
  EXPECT_EQ(2 - 1, reducer1.birth_levels_[2]);
  EXPECT_EQ(INFINITE_LEVEL, reducer1.death_levels_[2]);
  ASSERT_EQ(10, reducer1.upper_survivors()->size());

  Reducer<3, 2> reducer2(bitmap, reducer1.upper_survivors(), config);
  
  reducer2.compute();
  ASSERT_EQ(3, reducer2.birth_levels_.size());
  EXPECT_EQ(7 - 1, reducer2.birth_levels_[0]);
  EXPECT_EQ(INFINITE_LEVEL, reducer2.death_levels_[0]);
  EXPECT_EQ(6 - 1, reducer2.birth_levels_[1]);
  EXPECT_EQ(INFINITE_LEVEL, reducer2.death_levels_[1]);
  EXPECT_EQ(4 - 1, reducer2.birth_levels_[2]);
  EXPECT_EQ(INFINITE_LEVEL, reducer2.death_levels_[2]);
  ASSERT_EQ(1, reducer2.upper_survivors()->size());

  Reducer<3, 3> reducer3(bitmap, reducer2.upper_survivors(), config);
  
  reducer3.compute();
  ASSERT_EQ(1, reducer3.birth_levels_.size());
  EXPECT_EQ(8 - 1, reducer3.birth_levels_[0]);
  EXPECT_EQ(INFINITE_LEVEL, reducer3.death_levels_[0]);

}

TEST_P(ReducerTest, compute_2d_1) {
  Config config = GetParam();
  
  Bitmap<2> bitmap(std::vector<int>{3, 3});
  bitmap.load({
      0, 1, 2,
      6, 8, 7,
      3, 4, 5
    });
  LinkFind<2> link_find(bitmap);
  link_find.compute();
  Reducer<2, 1> reducer(bitmap, link_find.survivors_, config);
  reducer.compute();

  ASSERT_EQ(1, reducer.birth_levels_.size());
  EXPECT_EQ(7, reducer.birth_levels_[0]);
  EXPECT_EQ(8, reducer.death_levels_[0]);
  ASSERT_EQ(0, reducer.upper_survivors()->size());
}

TEST_P(ReducerTest, compute_2d_2) {
  Config config = GetParam();
  static const auto INFINITE_LEVEL = BasicTypes<2>::INFINITE_LEVEL;
  using Period = typename BasicTypes<2>::Period;

  Bitmap<2> bitmap(std::vector<int>{2, 2}, Period(0b11));
  bitmap.load({
      0, 2,
      3, 1,
    });
  LinkFind<2> link_find(bitmap);
  link_find.compute();
  Reducer<2, 1> reducer1(bitmap, link_find.survivors_, config);
  reducer1.compute();
  Reducer<2, 2> reducer2(bitmap, reducer1.upper_survivors(), config);
  reducer2.compute();

    
  ASSERT_EQ(2, link_find.birth_levels_.size());
  EXPECT_EQ(0, link_find.birth_levels_[0]);
  EXPECT_EQ(INFINITE_LEVEL, link_find.death_levels_[0]);
  EXPECT_EQ(1, link_find.birth_levels_[1]);
  EXPECT_EQ(2, link_find.death_levels_[1]);
  ASSERT_EQ(2, reducer1.birth_levels_.size());
  EXPECT_EQ(2, reducer1.birth_levels_[0]);
  EXPECT_EQ(INFINITE_LEVEL, reducer1.death_levels_[0]);
  EXPECT_EQ(2, reducer1.birth_levels_[1]);
  EXPECT_EQ(INFINITE_LEVEL, reducer1.death_levels_[1]);
  ASSERT_EQ(1, reducer2.birth_levels_.size());
  EXPECT_EQ(3, reducer2.birth_levels_[0]);
  EXPECT_EQ(INFINITE_LEVEL, reducer2.death_levels_[0]);
}

INSTANTIATE_TEST_CASE_P(ParameterizedTest, ReducerTest,
                        ::testing::Values(Config{CacheStrategy::ALWAYS, 0},
                                          Config{CacheStrategy::REQUESTED, 0},
                                          Config{CacheStrategy::NOTHING, 0}));

         
                        
