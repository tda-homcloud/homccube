# HomCCube

This is software for computing persistence diagrams for 2D and 3D cubical filtration.

## How to build

1. Install cmake and boost library
2. Type `cmake .` to generate Makefile
3. Type `make` to build
4. Type `make test` for unit tests
5. A generated file `cr` is an executable file

## How to run `cr`

The commandline operation is as follows:

    ./cr requested input.complex output.diagram
    
where `input.complex` is the dipha's bitmap data file and `output.diagram` is a
persistence diagram whose format is dipha's format.

The keyword of `requested` specifies internal algorithm.
You can also specify `always` and `nothing`. On the average, `requested` algorithm
gives best performance.

## Acknowledgement

Some mathematical and technical ideas are based on
CubicalRipser <https://github.com/CubicalRipser> by Takeshi Sudo and
Kazushi Ahara. 

I also thank Kaji-san for the discussion about the performance of CubicalRipser.

## License

This software is distributed under GPL version 3, or any later version.

## Author

Ippei Obayashi <ippei.obayashi@riken.jp>


