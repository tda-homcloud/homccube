#include <gtest/gtest.h>
#include "cube.hpp"

#include <sstream>

using namespace homccube;

TEST(Cube, Cube) {
  ASSERT_EQ(8, sizeof(Cube<3, 0>));
  ASSERT_EQ(8, sizeof(Cube<3, 1>));
  ASSERT_EQ(8, sizeof(Cube<3, 2>));
  ASSERT_EQ(8, sizeof(Cube<3, 3>));
}

TEST(Cube, io) {
  auto C = BasicTypes<3>::build_coord;
  std::stringstream ss;
  ss << Cube<3, 1>(C({32, 12, -1}), 0b100, 432);
  EXPECT_EQ(std::string("Cube<3,1>[432 100 {32,12,-1}]"), ss.str());
}
