#include <gtest/gtest.h>
#include "bitmap.hpp"

#include <algorithm>
#include <vector>

using namespace homccube;
using Period = BasicTypes<3>::Period;

TEST(Bitmap, Bitmap) {
  Bitmap<3> bitmap(std::vector<int>{3, 3, 3});
}

TEST(Bitmap, load) {
  Bitmap<3> bitmap(std::vector<int>{2, 2, 1});
  bitmap.load({0.5, -1.0, 1.0, 0.8});
  ASSERT_EQ(4, bitmap.levels_.size());
  EXPECT_TRUE(std::equal(bitmap.levels_.begin(), bitmap.levels_.end(),
                         std::vector<std::uint32_t>({1, 0, 3, 2}).begin()));
}

TEST(Bitmap, values) {
  const auto C = BasicTypes<3>::build_coord;
  Bitmap<3> bitmap(std::vector<int>{2, 2, 2});
  bitmap.load({1.1, 0.1,
               3.1, 2.1,

               4.1, 7.1,
               5.1, 6.1,});
  EXPECT_EQ(7.1, bitmap.values(C({1, 0, 1})));
  EXPECT_EQ(5.1, bitmap.values(C({1, 1, 0})));
  EXPECT_EQ(0.1, bitmap.values(C({2, 0, 1})));
  EXPECT_EQ(1.1, bitmap.values(C({2, 2, 2})));
}

TEST(Bitmap, level2values) {
  Bitmap<3> bitmap(std::vector<int>{2, 2, 2});
  bitmap.load({1.1, 0.1,
               3.1, 2.1,

               4.1, 7.1,
               5.1, 6.1,});
  ASSERT_EQ(bitmap.level2value_.size(), 8);
  for (int i = 0; i < 8; ++i)
    EXPECT_EQ(0.1 + i, bitmap.level2value_[i]) << i;
}

TEST(Bitmap, levels) {
  const auto C = BasicTypes<3>::build_coord;
  Bitmap<3> bitmap(std::vector<int>{2, 2, 2});
  bitmap.load({1.0, 0.0,
               3.0, 2.0,

               4.0, 7.0,
               5.0, 6.0,});
  EXPECT_EQ(7, bitmap.levels(C({1, 0, 1})));
  EXPECT_EQ(5, bitmap.levels(C({1, 1, 0})));
  EXPECT_EQ(0, bitmap.levels(C({2, 0, 1})));
  EXPECT_EQ(1, bitmap.levels(C({2, 2, 2})));
  EXPECT_EQ(0, bitmap.levels(C({2, 0, -1})));
  EXPECT_EQ(2, bitmap.levels(C({2, -1, 1})));
}

TEST(Bitmap, vertex) {
  const auto C = BasicTypes<3>::build_coord;
  Bitmap<3> bitmap(std::vector<int>{2, 2, 2});
  bitmap.load({1.0, 0.0,
               3.0, 2.0,

               4.0, 7.0,
               5.0, 6.0,});
  {
    Cube<3, 0> cube = bitmap.vertex(C({1, 1, 0}));
    ASSERT_EQ(5, cube.pixel_level_);
    ASSERT_EQ(0, cube.nd_);
  }
}

TEST(Bitmap, cube) {
  const auto C = BasicTypes<3>::build_coord;
  Bitmap<3> bitmap(std::vector<int>{2, 2, 2});
  bitmap.load({1.0, 0.0,
               3.0, 2.0,

               4.0, 7.0,
               5.0, 6.0,});
  {
    Cube<3, 0> cube = bitmap.cube<0>(C({1, 1, 0}), C({1, 1, 0}));
    EXPECT_EQ(1, cube.coord_[0]);
    EXPECT_EQ(1, cube.coord_[1]);
    EXPECT_EQ(0, cube.coord_[2]);
    EXPECT_EQ(5, cube.pixel_level_);
    EXPECT_EQ(0, cube.nd_);
  }
  {
    Cube<3, 1> cube = bitmap.cube<1>(C({1, 1, 0}), C({1, 1, 1}));
    ASSERT_EQ(1, cube.coord_[0]);
    ASSERT_EQ(1, cube.coord_[1]);
    ASSERT_EQ(0, cube.coord_[2]);
    EXPECT_EQ(6, cube.pixel_level_);
    EXPECT_EQ(0b100, cube.nd_);
  }
  {
    Cube<3, 1> cube = bitmap.cube<1>(C({1, 1, 1}), C({1, 1, 0}));
    EXPECT_EQ(1, cube.coord_[0]);
    EXPECT_EQ(1, cube.coord_[1]);
    EXPECT_EQ(0, cube.coord_[2]);
    EXPECT_EQ(6, cube.pixel_level_);
    EXPECT_EQ(0b100, cube.nd_);
  }
  {
    Cube<3, 3> cube = bitmap.cube<3>(C({1, 0, 1}), C({0, 1, 0}));
    EXPECT_EQ(0, cube.coord_[0]);
    EXPECT_EQ(0, cube.coord_[1]);
    EXPECT_EQ(0, cube.coord_[2]);
    EXPECT_EQ(7, cube.pixel_level_);
    EXPECT_EQ(0b111, cube.nd_);
  }

  {
    Cube<3, 3> cube = bitmap.cube<3>(C({2, 1, 1}), C({1, 2, 2}));
    EXPECT_EQ(1, cube.coord_[0]);
    EXPECT_EQ(1, cube.coord_[1]);
    EXPECT_EQ(1, cube.coord_[2]);
    EXPECT_EQ(7, cube.pixel_level_);
    EXPECT_EQ(0b111, cube.nd_);
  }

  {
    Cube<3, 3> cube1 = bitmap.cube<3>(C({2, 1, 1}), C({1, 2, 2}));
    Cube<3, 3> cube2 = bitmap.cube<3>(C({1, 1, 1}), C({2, 2, 2}));
    Cube<3, 3> cube3 = bitmap.cube<3>(C({-1, -1, -1}), C({0, 0, 0}));
    Cube<3, 3> cube4 = bitmap.cube<3>(C({-1, -1, 1}), C({0, 0, 2}));
    EXPECT_TRUE(cube1 == cube2);
    EXPECT_TRUE(cube1 == cube3);
    EXPECT_TRUE(cube1 == cube4);
  }
  
  {
    Cube<3, 2> cube = bitmap.cube<2>(C({-1, 0, 1}), C({0, 1, 1}));
    EXPECT_EQ(1, cube.coord_[0]);
    EXPECT_EQ(0, cube.coord_[1]);
    EXPECT_EQ(1, cube.coord_[2]);
    EXPECT_EQ(7, cube.pixel_level_);
    EXPECT_EQ(0b011, cube.nd_);
  }

#pragma GCC diagnostic ignored "-Wnarrowing"
  {
    std::vector<BasicTypes<3>::Level> levels;
    Coord<3> x = C({1, 1, 1});
    for (int dx = -1; dx <= 1; dx += 2)
      for (int dy = -1; dy <= 1; dy += 2)
        for (int dz = -1; dz <= 1; dz += 2) {
          Coord<3> y = C({x[0] + dx, x[1] + dy, x[2] + dz});
          Cube<3, 3> cube = bitmap.cube<3>(x, y);
          EXPECT_EQ(7, cube.pixel_level_);
          levels.push_back(cube.level_);
        }
    std::sort(levels.begin(), levels.end());
    for (int i = 0; i < 7; ++i) {
      EXPECT_TRUE(levels[i] != levels[i + 1]);
    }
  }
}

TEST(Bitmap, cofaces3d) {
  const auto C = BasicTypes<3>::build_coord;
  Bitmap<3> bitmap(std::vector<int>{2, 2, 2});
  bitmap.load({1.0, 0.0,
               3.0, 2.0,

               4.0, 7.0,
               5.0, 6.0,});
  {
    Cube<3, 3> cube = bitmap.cube<3>(C({0, 0, 0}), C({1, 1, 1}));
    std::vector<Cube<3, 4>> cofaces = bitmap.cofaces(cube);
    ASSERT_EQ(0, cofaces.size());
  }

  {
    Cube<3, 0> cube = bitmap.vertex(C({0, 1, 0}));
    std::vector<Cube<3, 1>> cofaces = bitmap.cofaces(cube);
    ASSERT_EQ(3, cofaces.size());
    EXPECT_EQ(bitmap.cube<1>(C({0, 1, 0}), C({1, 1, 0})), cofaces[0]);
    EXPECT_EQ(bitmap.cube<1>(C({0, 1, 0}), C({0, 0, 0})), cofaces[1]);
    EXPECT_EQ(bitmap.cube<1>(C({0, 1, 0}), C({0, 1, 1})), cofaces[2]);
  }
  {
    Cube<3, 1> cube = bitmap.cube<1>(C({0, 1, 0}), C({0, 1, 1}));
    std::vector<Cube<3, 2>> cofaces = bitmap.cofaces(cube);
    ASSERT_EQ(2, cofaces.size());
    EXPECT_EQ(bitmap.cube<2>(C({0, 1, 0}), C({1, 1, 1})), cofaces[0]);
    EXPECT_EQ(bitmap.cube<2>(C({0, 1, 0}), C({0, 0, 1})), cofaces[1]);
  }
  {
    Cube<3, 2> cube = bitmap.cube<2>(C({0, 1, 0}), C({1, 1, 1}));
    std::vector<Cube<3, 3>> cofaces = bitmap.cofaces(cube);
    ASSERT_EQ(1, cofaces.size());
    EXPECT_EQ(bitmap.cube<3>(C({0, 1, 0}), C({1, 0, 1})), cofaces[0]);
  }
}

TEST(Bitmap, cofnaces2d) {
  const auto C = BasicTypes<2>::build_coord;
  Bitmap<2> bitmap(std::vector<int>{3, 3});
  bitmap.load({
      1, 2, 3,
      4, 5, 6,
      7, 8, 9,});
  {
    Cube<2, 0> cube = bitmap.vertex(C({1, 1}));
    std::vector<Cube<2, 1>> cofaces = bitmap.cofaces(cube);
    ASSERT_EQ(4, cofaces.size());

    EXPECT_EQ(bitmap.cube<1>(C({1, 1}), C({2, 1})), cofaces[0]);
    EXPECT_EQ(bitmap.cube<1>(C({1, 1}), C({0, 1})), cofaces[1]);
    EXPECT_EQ(bitmap.cube<1>(C({1, 1}), C({1, 2})), cofaces[2]);
    EXPECT_EQ(bitmap.cube<1>(C({1, 1}), C({1, 0})), cofaces[3]);
  }
  {
    Cube<2, 1> cube = bitmap.cube<1>(C({0, 0}), C({0, 1}));
    std::vector<Cube<2, 2>> cofaces = bitmap.cofaces(cube);
    ASSERT_EQ(1, cofaces.size());
    EXPECT_EQ(bitmap.cube<2>(C({0, 0}), C({1, 1})), cofaces[0]);
  }
  {
    Cube<2, 1> cube = bitmap.cube<1>(C({1, 0}), C({1, 1}));
    std::vector<Cube<2, 2>> cofaces = bitmap.cofaces(cube);
    ASSERT_EQ(2, cofaces.size());
    EXPECT_EQ(bitmap.cube<2>(C({1, 0}), C({2, 1})), cofaces[0]);
    EXPECT_EQ(bitmap.cube<2>(C({0, 0}), C({1, 1})), cofaces[1]);
  }

}

TEST(Bitmap, cofaces_periodic) {
  const auto C = BasicTypes<3>::build_coord;
  Bitmap<3> bitmap(std::vector<int>{2, 2, 2}, Period(0b110));
  bitmap.load({1.0, 0.0,
               3.0, 2.0,

               4.0, 7.0,
               5.0, 6.0,});
  {
    Cube<3, 3> cube = bitmap.cube<3>(C({0, 0, 0}), C({1, 1, 1}));
    std::vector<Cube<3, 4>> cofaces = bitmap.cofaces(cube);
    ASSERT_EQ(0, cofaces.size());
  }

  {
    Cube<3, 0> cube = bitmap.vertex(C({0, 1, 0}));
    std::vector<Cube<3, 1>> cofaces = bitmap.cofaces(cube);
    ASSERT_EQ(5, cofaces.size());
    EXPECT_EQ(bitmap.cube<1>(C({0, 1, 0}), C({1, 1, 0})), cofaces[0]);
    EXPECT_EQ(bitmap.cube<1>(C({0, 1, 0}), C({0, 2, 0})), cofaces[1]);
    EXPECT_EQ(bitmap.cube<1>(C({0, 1, 0}), C({0, 0, 0})), cofaces[2]);
    EXPECT_EQ(bitmap.cube<1>(C({0, 1, 0}), C({0, 1, 1})), cofaces[3]);
    EXPECT_EQ(bitmap.cube<1>(C({0, 1, 1}), C({0, 1, 2})), cofaces[4]);
  }

  {
    Cube<3, 1> cube = bitmap.cube<1>(C({0, 1, 0}), C({1, 1, 0}));
    std::vector<Cube<3, 2>> cofaces = bitmap.cofaces(cube);
    ASSERT_EQ(4, cofaces.size());
    EXPECT_EQ(bitmap.cube<2>(C({0, 1, 0}), C({1, 2, 0})), cofaces[0]);
    EXPECT_EQ(bitmap.cube<2>(C({0, 0, 0}), C({1, 1, 0})), cofaces[1]);
    EXPECT_EQ(bitmap.cube<2>(C({0, 1, 0}), C({1, 1, 1})), cofaces[2]);
    EXPECT_EQ(bitmap.cube<2>(C({0, 1, 1}), C({1, 1, 2})), cofaces[3]);
  }
  {
    Cube<3, 2> cube = bitmap.cube<2>(C({0, 1, 1}), C({1, 1, 2}));
    std::vector<Cube<3, 3>> cofaces = bitmap.cofaces(cube);
    ASSERT_EQ(2, cofaces.size());
    EXPECT_EQ(bitmap.cube<3>(C({0, 1, 1}), C({1, 2, 2})), cofaces[0]);
    EXPECT_EQ(bitmap.cube<3>(C({0, 0, 1}), C({1, 1, 2})), cofaces[1]);
  }
  {
    Cube<3, 2> cube = bitmap.cube<2>(C({0, 1, 1}), C({0, 2, 2}));
    std::vector<Cube<3, 3>> cofaces = bitmap.cofaces(cube);
    ASSERT_EQ(1, cofaces.size());
    EXPECT_EQ(bitmap.cube<3>(C({0, 1, 1}), C({1, 2, 2})), cofaces[0]);
  }
}

TEST(Bitmap, neighbours) {
  const auto C = BasicTypes<3>::build_coord;
  Bitmap<3> bitmap(std::vector<int>{2, 2, 2}, Period(0));
  bitmap.load({1.0, 0.0,
               3.0, 2.0,

               4.0, 7.0,
               5.0, 6.0,});
  std::vector<Coord<3>> neighbours;
  bitmap.neighbours(C({0, 1, 0}), neighbours);
  ASSERT_EQ(3, neighbours.size());
  EXPECT_EQ(C({0, 0, 0}), neighbours[0]);
  EXPECT_EQ(C({1, 1, 0}), neighbours[1]);
  EXPECT_EQ(C({0, 1, 1}), neighbours[2]);
}

TEST(Bitmap, neighbours_periodic) {
  const auto C = BasicTypes<3>::build_coord;
  Bitmap<3> bitmap(std::vector<int>{3, 4, 5}, Period(0b110));
  std::fill(bitmap.levels_.begin(), bitmap.levels_.end(), 0);
  bitmap.levels(C({0, 1, 0})) = 5;
  std::vector<Coord<3>> neighbours;
  bitmap.neighbours(C({0, 1, 0}), neighbours);
  ASSERT_EQ(5, neighbours.size());
  EXPECT_EQ(C({0, 0, 0}), neighbours[0]);
  EXPECT_EQ(C({0, 1, -1}), neighbours[1]);
  EXPECT_EQ(C({1, 1, 0}), neighbours[2]);
  EXPECT_EQ(C({0, 2, 0}), neighbours[3]);
  EXPECT_EQ(C({0, 1, 1}), neighbours[4]);
}

TEST(Bitmap, index2coord) {
  const auto C = BasicTypes<3>::build_coord;
  Bitmap<3> bitmap(std::vector<int>{4, 3, 5}, Period(0b110));
  EXPECT_EQ((C({1, 2, 4})), bitmap.index2coord(bitmap.coord2index(C({1, 2, 4}))));
  EXPECT_EQ(28, bitmap.coord2index(bitmap.index2coord(28)));
}

TEST(Bitmap, sorted_vertices) {
  Bitmap<3> bitmap(std::vector<int>{2, 2, 2}, Period(0b110));
  bitmap.load({1.0, 0.0,
               3.0, 2.0,

               4.0, 7.0,
               5.0, 6.0,});
  auto vertices = bitmap.sorted_vertices();
  ASSERT_EQ(8, vertices.size());
  for (int i = 0; i < 8; ++i)
    EXPECT_EQ(i, vertices[i].pixel_level_);
}

TEST(Bitmap, foreach_cube) {
  const auto C = BasicTypes<3>::build_coord;
  {
    Bitmap<3> bitmap(std::vector<int>{2, 2, 2});
    bitmap.load({1.0, 0.0,
                 3.0, 2.0,

                 4.0, 7.0,
                 5.0, 6.0,});
    
    std::vector<Cube<3, 1>> output;
    bitmap.foreach_cube<1>([&output](Cube<3, 1> x) { output.push_back(x); });
    ASSERT_EQ(12, output.size());
    std::sort(output.begin(), output.end());
    EXPECT_EQ(bitmap.cube<1>(C({0, 0, 0}), C({0, 0, 1})), output[0]);
    EXPECT_EQ(bitmap.cube<1>(C({0, 0, 1}), C({0, 1, 1})), output[1]);
    EXPECT_EQ(bitmap.cube<1>(C({0, 1, 0}), C({0, 0, 0})), output[2]);
    EXPECT_EQ(bitmap.cube<1>(C({0, 1, 0}), C({0, 1, 1})), output[3]);
    EXPECT_EQ(bitmap.cube<1>(C({1, 0, 0}), C({0, 0, 0})), output[4]);
    EXPECT_EQ(bitmap.cube<1>(C({1, 1, 0}), C({0, 1, 0})), output[5]);
    EXPECT_EQ(bitmap.cube<1>(C({1, 1, 0}), C({1, 0, 0})), output[6]);
    EXPECT_EQ(bitmap.cube<1>(C({1, 1, 1}), C({0, 1, 1})), output[7]);
    EXPECT_EQ(bitmap.cube<1>(C({1, 1, 1}), C({1, 1, 0})), output[8]);
    EXPECT_EQ(bitmap.cube<1>(C({1, 0, 1}), C({0, 0, 1})), output[9]);
    EXPECT_EQ(bitmap.cube<1>(C({1, 0, 1}), C({1, 1, 1})), output[10]);
    EXPECT_EQ(bitmap.cube<1>(C({1, 0, 1}), C({1, 0, 0})), output[11]);
  }
  {
    Bitmap<3> bitmap(std::vector<int>{2, 2, 2});
    std::vector<Cube<3, 2>> output;
    bitmap.foreach_cube<2>([&output](Cube<3, 2> x) { output.push_back(x); });
    ASSERT_EQ(6, output.size());
  }
  {
    Bitmap<3> bitmap(std::vector<int>{2, 2, 2});
    std::vector<Cube<3, 3>> output;
    bitmap.foreach_cube<3>([&output](Cube<3, 3> x) { output.push_back(x); });
    ASSERT_EQ(1, output.size());
    ASSERT_EQ(bitmap.cube<3>(C({0, 0, 0}), C({1, 1, 1})), output[0]);
  }
  {
    Bitmap<3> bitmap(std::vector<int>{2, 2, 2}, Period(0b101));
    bitmap.load({1.0, 0.0,
                 3.0, 2.0,

                 4.0, 7.0,
                 5.0, 6.0,});

    std::vector<Cube<3, 3>> output;
    bitmap.foreach_cube<3>([&output](Cube<3, 3> x) { output.push_back(x); });
    std::sort(output.begin(), output.end());
    ASSERT_EQ(4, output.size());
    for (const auto& cube: output) {
      EXPECT_EQ(7, cube.pixel_level_);
    }
    EXPECT_EQ(bitmap.cube<3>(C({0, 0, 0}), C({1, 1, 1})), output[0]);
    EXPECT_EQ(bitmap.cube<3>(C({1, 0, 0}), C({2, 1, 1})), output[1]);
    EXPECT_EQ(bitmap.cube<3>(C({0, 0, 1}), C({1, 1, 2})), output[2]);
    EXPECT_EQ(bitmap.cube<3>(C({1, 0, 1}), C({2, 1, 2})), output[3]);
  }
}
