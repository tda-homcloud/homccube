import struct
import sys
import numpy as np

def main(output, shape):
    bitmap = np.random.uniform(0, 1.0, size=shape)
    with open(output + ".complex", "wb") as f:
        f.write(struct.pack("qq", 8067171840, 1))
        f.write(struct.pack("qq", bitmap.size, bitmap.ndim))

        for g in reversed(bitmap.shape):
            f.write(struct.pack("q", g))

        for pixel in bitmap.flatten():
            f.write(struct.pack("d", pixel))

    np.save(output + ".npy", bitmap)

if __name__ == '__main__':
    main(sys.argv[1], [int(n) for n in sys.argv[2:]])

